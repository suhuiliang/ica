This solution is based on Mariadb server. A docker compose file is prepared
to simply start the server. 

```sh
# start server
docker-compose up -d

# login to server
docker exec -it ica-db-1 bash

# Initial db, which create tables and load tsv data into tables
# password is `password`
mysql -uica -Dica -p < script/initdb.sql

# login client
mysql -u ica -D ica -p
```
Solution is located in `scripts/q.sql`
Index and Data type is not optimized.


## Example of output of Question 1:

```
+---------------+-------------+------+-----------+-----------+--------------------+------------+---------+---------+---------+---------+---------+---------+---------+
| customer_name | customer_id | age  | num_offer | num_sales | total_sales        | num_redeem | num_mon | num_tue | num_wed | num_thu | num_fri | num_sat | num_sun |
+---------------+-------------+------+-----------+-----------+--------------------+------------+---------+---------+---------+---------+---------+---------+---------+
| xxxx          |      828745 |   81 |      NULL |        13 | 3418.3851985343094 |         98 |       0 |       1 |       5 |       3 |       3 |       1 |       0 |
| xxxx          |      828774 |   77 |      NULL |        17 |  42009.60734727325 |        388 |       0 |       2 |       3 |       5 |       4 |       1 |       2 |
| xxxx          |      828938 |   57 |      NULL |         9 |   1467.71136625295 |         22 |       1 |       0 |       1 |       1 |       4 |       1 |       1 |
| xxxx          |      828947 |   66 |      NULL |         0 |               NULL |          0 |       0 |       0 |       0 |       0 |       0 |       0 |       0 |
| xxxx          |      828958 |   75 |      NULL |         0 |               NULL |          0 |       0 |       0 |       0 |       0 |       0 |       0 |       0 |
| xxxx          |      828991 |   67 |      NULL |        22 | 11547.009654990487 |        126 |       2 |       4 |       1 |       7 |       4 |       3 |       1 |
| xxxx          |      829342 |   65 |      NULL |        13 | 2966.4693644547406 |         31 |       3 |       0 |       4 |       0 |       4 |       2 |       0 |
| xxxx          |      829362 |   70 |      NULL |         1 |        11.94323996 |          1 |       1 |       0 |       0 |       0 |       0 |       0 |       0 |
| xxxx          |      834981 |   67 |        32 |        11 | 2670.4498392559994 |         16 |       1 |       0 |       3 |       5 |       1 |       0 |       1 |
| xxxx          |      834993 |   75 |      NULL |         0 |               NULL |          0 |       0 |       0 |       0 |       0 |       0 |       0 |       0 |
+---------------+-------------+------+-----------+-----------+--------------------+------------+---------+---------+---------+---------+---------+---------+---------+
```

\* `num_offer`: offers which only listed in `offers` table. 
\* `num_redeem`: included offers which are not included in `offers` table.

## Output of Question 2:

```
+-----------+------+
| age_group | dow  |
+-----------+------+
| 40+       |    5 |
| 50+       |    6 |
| 60+       |    3 |
| 70+       |    3 |
| 80+       |    3 |
| 90+       |    3 |
+-----------+------+
```

\* dow: weekday index for a given date (a number from 1 to 7). Note: 1=Sunday, 2=Monday, 3=Tuesday, 4=Wednesday, 5=Thursday, 6=Friday, 7=Saturday 


# Notice:
one transaction mapped to multiple customers on different days: 34786846


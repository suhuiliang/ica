-- ########################
-- Question 1
-- ########################
select 
  c.customer_name, 
  c.customer_id, 
  timestampdiff(year, customer_dob, curdate()) as age, 
  o.num_offer,
  count(distinct transaction_id) as num_sales,
  sum(sales_amount) total_sales, 
  count(t.offer_id) as num_redeem, -- Can add distinct based on business logic
  count(distinct(if(dayofweek(t.transaction_date) = 2, transaction_id, NULL))) as num_mon,
  count(distinct(if(dayofweek(t.transaction_date) = 3, transaction_id, NULL))) as num_tue,
  count(distinct(if(dayofweek(t.transaction_date) = 4, transaction_id, NULL))) as num_wed,
  count(distinct(if(dayofweek(t.transaction_date) = 5, transaction_id, NULL))) as num_thu,
  count(distinct(if(dayofweek(t.transaction_date) = 6, transaction_id, NULL))) as num_fri,
  count(distinct(if(dayofweek(t.transaction_date) = 7, transaction_id, NULL))) as num_sat,
  count(distinct(if(dayofweek(t.transaction_date) = 1, transaction_id, NULL))) as num_sun
from customers c 
left join transactions t 
  on c.customer_id = t.customer_id
left join 
  (select 
    customer_id, 
    count(offer_id) as num_offer
    from offers
    group by customer_id) as o
  on c.customer_id = o.customer_id
group by c.customer_id;

-- ###################################
-- Most visited dow of each age group
-- ###################################

-- option 1 (This solution only works on MySql/MariaDB. Option 2 for general db):

with visit_per_agegroup_dow as (
    select
        age_group,
        dow,
        count(*) as num_visit
    from
        (
            select
                distinct t.transaction_id,
                c.customer_id,
                timestampdiff(year, customer_dob, curdate()) as age,
                floor(
                    timestampdiff(year, customer_dob, curdate()) / 10
                ) as age_group,
                dayofweek(t.transaction_date) as dow
            from
                transactions t
                join customers c on c.customer_id = t.customer_id
            order by
                transaction_id
        ) as tmp
    group by
        age_group,
        dow
    order by
        num_visit desc
)
select
    age_group,
    dow num_visit
from
    visit_per_agegroup_dow
group by
    age_group


-- option 2 with window function:

with visit_per_agegroup_dow as (
    select
        age_group,
        dow,
        count(*) as num_visit
    from
        (
            select
                distinct t.transaction_id,
                c.customer_id,
                timestampdiff(year, customer_dob, curdate()) as age,
                concat(
                    floor(
                        timestampdiff(year, customer_dob, curdate()) / 10
                    )*10,
                    '+'
                ) as age_group,
                dayofweek(t.transaction_date) as dow
            from
                transactions t
                join customers c on c.customer_id = t.customer_id
            order by
                transaction_id
        ) as tmp
    group by
        age_group,
        dow
),
visit_per_agegroup_dow_rank as (
    select
        age_group,
        dow,
        num_visit,
        rank() over (
            partition by age_group
            order by
                num_visit desc
        ) as rank
    from
        visit_per_agegroup_dow
)
select
    age_group,
    dow
from
    visit_per_agegroup_dow_rank
where
    rank = 1;

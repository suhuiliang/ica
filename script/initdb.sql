drop table if exists customers;
create table if not exists customers(
  customer_name varchar(255),
  customer_id int not null,
  customer_dob date,
  gender varchar(255),
  primary key(customer_id)
);

drop table if exists transactions;
create table if not exists transactions(
  transaction_id int not null ,
  customer_id int,
  product_id int, 
  store_id int,
  offer_id bigint unsigned default null,
  sales_amount double,
  transaction_date date,
  index (customer_id)
);

drop table if exists offers;
create table if not exists offers (
  customer_id int,
  offer_id bigint unsigned,
  start_date date,
  end_date date,
  primary key(offer_id, customer_id)

);

load data local 
infile 'resources/customers.tsv'
into table customers
columns terminated by '\t'
lines terminated by '\n'
ignore 1 rows;

load data local
infile 'resources/transactions.tsv'
into table transactions
columns terminated by '\t'
lines terminated by '\n'
ignore 1 rows
(transaction_id, customer_id, product_id, store_id, @voffer_id, sales_amount, transaction_date)
SET offer_id = NULLIF(@voffer_id,'');


load data local
infile 'resources/offers.tsv'
into table offers
columns terminated by '\t'
lines terminated by '\n'
ignore 1 rows;
